// title      : cubeHook
// author     : John Cole
// license    : ISC License
// file       : cubeHook.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: 'part',
      type: 'choice',
      values: ['partHook', 'partShelf'],
      captions: ['hook', 'shelf'],
      initial: 0,
      caption: 'Part:'
    },
    { type: 'group', name: 'Shelf' },
    {
      name: 'shelfWidth',
      caption: 'Width (inch):',
      type: 'float',
      initial: 4.0
    },
    {
      name: 'bracketThickness',
      caption: 'Thickness (inch):',
      type: 'float',
      initial: 0.75
    },
    {
      name: 'screwSize',
      type: 'choice',
      values: ['#4', '#6', '#8'],

      initial: 1,
      caption: 'Screw Size:'
    },
    {
      name: 'side',
      type: 'choice',
      values: ['left', 'right'],

      initial: 0,
      caption: 'Side:'
    },
    { type: 'group', name: 'Parameters' },
    {
      name: 'thickness',
      caption: 'Tennon Thickness:',
      type: 'float',
      initial: 3.35
    },
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3, 4],
      captions: [
        'very low (6,16)',
        'low (8,24)',
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)'
      ],
      initial: 2,
      caption: 'Resolution:'
    }
  ];
}

function main(params) {
  var resolutions = [[6, 16], [8, 24], [12, 32], [24, 64], [48, 128]];
  CSG.defaultResolution3D = resolutions[params.resolution][0];
  CSG.defaultResolution2D = resolutions[params.resolution][1];

  util.init(CSG);

  var delta = 0.0;

  var parts = {
    partHook,
    partShelf
  };

  return parts[params.part]();

  function partHook() {
    var hook_shape = Hook();
    var hook = util
      .poly2solid(hook_shape, hook_shape, params.thickness + delta)
      .fit([40, 55, 0]);
    var label = util
      .label(delta.toFixed(1))
      .fit(10, 10, 0, true)
      .snap(hook, 'z', 'outside-')
      .align(hook, 'xy')
      .translate([-2.5, -10, 0])
      .color('blue');
    return hook;
    // var gap = Parts.Cube([3, 17, params.thickness]).color('red');
    // var gap2 = Parts.Cube([5, 13, params.thickness]).translate([-5, 0, 0]);
    // return [
    //   hook.translate([15, 16, -params.thickness]).color('gray', 0.5),
    //   gap,
    //   gap2
    // ];
  }

  function partShelf() {
    var hook_shape = ShelfHook();
    var hookHeight = 110.4; //util.inch(22 * 0.25);
    var hook = util
      .poly2solid(hook_shape, hook_shape, params.thickness + delta)
      .fit([21.1, hookHeight, 0])
      .Center()
      .color('blue');
    var shelfOffset = 7.15;
    var mount = polyhedron({
      points: [
        [0, 0, 0],
        [0, hookHeight - shelfOffset, 0],
        [util.inch(params.shelfWidth), hookHeight - shelfOffset, 0],
        [0, 0, params.thickness + delta],
        [0, hookHeight - shelfOffset, params.thickness + delta],
        [
          util.inch(params.shelfWidth),
          hookHeight - shelfOffset,
          util.inch(params.bracketThickness)
        ]
      ],
      triangles: [
        [2, 1, 0],
        [3, 4, 5],
        [0, 1, 3],
        [1, 4, 3],
        [1, 2, 4],
        [5, 4, 2],
        [2, 0, 5],
        [0, 3, 5]
      ]
    })
      .snap(hook, 'x', 'outside-')
      .snap(hook, 'y', 'inside-')
      .color('orange', 0.9);

    var bolt = Hardware.PanHeadScrew(
      ...ImperialWoodScrews[params.screwSize],
      util.inch(0.5),
      util.inch(2)
    )
      .rotate('head', 'x', -90)
      .snap('head', mount, 'xy', 'inside+')
      .align('head', mount, 'z')
      .translate([-util.inch(0.5), -util.inch(0.25), 0]);

    var bolt2 = bolt
      .clone()
      .snap('head', mount, 'x', 'inside-')
      .translate([util.inch(0.5), 0, 0]);

    var supportDiameter =
      ImperialWoodScrews[params.screwSize][0] + util.inch(1 / 8);
    var supportRadius = supportDiameter / 2;
    var bolt2support = util.group();
    bolt2support.add(
      Parts.Cylinder(supportDiameter, 10)
        .rotateX(90)
        .color('green'),
      'cyl'
    );
    bolt2support.add(
      CSG.cube({
        center: [0, -5, -4],
        radius: [supportRadius, 5, 4]
      })
        .translate([0, 0, 0])
        .color('orange'),
      'base'
    );

    bolt2support
      .align('cyl', bolt2.parts.head, 'xyz')
      .snap('cyl', mount, 'y', 'inside+');

    var side = {
      left: () => {
        return mount
          .union(bolt2support.combine())
          .subtract([bolt.combineAll(), bolt2.combineAll()]);
      },

      right: () => {
        return mount
          .union(bolt2support.combine())
          .subtract([bolt.combineAll(), bolt2.combineAll()])
          .mirroredZ()
          .snap(hook, 'z', 'inside+');
      }
    };

    var roundSize = util.inch(0.25);
    var round = Parts.Cube([roundSize / 2, 10, roundSize / 2])
      .subtract(
        Parts.Cylinder(roundSize, 10)
          .rotateX(90)
          .translate([0, 10, params.side == 'right' ? 0 : roundSize / 2])
      )
      .snap(mount, 'x', 'outside-')
      .snap(mount, 'y', 'inside+')

      .translate([-roundSize / 2, 0, params.side == 'right' ? 0.175 : 0]);
    return [union([hook, side[params.side]()]).subtract(round)];
  }
}
// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
